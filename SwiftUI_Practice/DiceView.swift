//
//  DiceView.swift
//  SwiftUI_Practice
//
//  Created by Sachin Khanal on 5/21/20.
//  Copyright © 2020 Sachin Khanal. All rights reserved.
//

import SwiftUI

struct DiceView: View {
    
    @Binding var diceFace: String
    @Binding var background: Color
    var body: some View {
        Image(diceFace).resizable().padding().background(background.opacity(0.1)).aspectRatio(1, contentMode: .fit).cornerRadius(20)
    }
}

struct DiceView_Previews: PreviewProvider {
    static var previews: some View {
        DiceView(diceFace: Binding.constant("dice-face-one"), background: Binding.constant(Color.red))
    }
}
