//
//  ContentView.swift
//  SwiftUI_Practice
//
//  Created by Sachin Khanal on 5/19/20.
//  Copyright © 2020 Sachin Khanal. All rights reserved.
//

import SwiftUI
let imageWidth = UIScreen.main.bounds.width
let imageHeight = UIScreen.main.bounds.height
struct ContentView: View {
    @State private var dices = ["dice-face-one", "dice-face-two", "dice-face-three", "dice-face-four", "dice-face-five", "dice-face-six"]
    @State private var numbers = [2, 0, 0,2,2,2,2,2,2]
    @State private var backgrounds = [Color.white, Color.white, Color.white,Color.white, Color.white, Color.white,Color.white, Color.white, Color.white]
    @State private var initialAmount = 2000
    @State private var bettingAmount = 5
    var body: some View {
        ZStack {
            VStack {
                Image("Background")
                    .resizable()
                    .edgesIgnoringSafeArea(.all)
                    .frame(maxWidth: .infinity, maxHeight: .infinity)
            }
            
            VStack(spacing: 25) {
                Rectangle().opacity(0.2).foregroundColor(Color.red)
                Rectangle().opacity(0.2).foregroundColor(Color.red)
                Rectangle().opacity(0.2).foregroundColor(Color.red)
                Rectangle().opacity(0.2).foregroundColor(Color.red)
                Rectangle().opacity(0.2).foregroundColor(Color.red)
            }
            HStack {
                Rectangle().opacity(0.2).foregroundColor(Color.red)
                Rectangle().opacity(0.2).foregroundColor(Color.red)
                Rectangle().opacity(0.2).foregroundColor(Color.red)
            }.edgesIgnoringSafeArea(.all)
            
            VStack {
                HStack(alignment: .center) {
                    VStack(alignment: .leading) {
                        Text("LUCKY DICE")
                            .font(.largeTitle)
                            .fontWeight(.heavy)
                            .foregroundColor(Color.white)
                    }
                    .padding()
                    Spacer()
                    Image(dices[1])
                        .resizable()
                        .padding(.trailing)
                        .frame(width: 60.0, height: 45)
                }
                Spacer()
                Text("Balance: \(initialAmount)").font(.largeTitle)
                    .fontWeight(.medium)
                    .foregroundColor(Color.white)
                    .padding(.all, 10)
                    .padding([.trailing, .leading], 20)
                    .background(Color.purple)
                    .cornerRadius(25)
                Spacer()
                VStack{
                    HStack {
                        Spacer()
                        DiceView(diceFace: $dices[numbers[0]], background: $backgrounds[0])
                        DiceView(diceFace: $dices[numbers[1]], background: $backgrounds[1])
                        DiceView(diceFace: $dices[numbers[2]], background: $backgrounds[2])
                        Spacer()
                    }
                    HStack {
                        Spacer()
                        DiceView(diceFace: $dices[numbers[3]], background: $backgrounds[3])
                        DiceView(diceFace: $dices[numbers[4]], background: $backgrounds[4])
                        DiceView(diceFace: $dices[numbers[5]], background: $backgrounds[5])
                        Spacer()
                    }
                    HStack {
                        Spacer()
                        DiceView(diceFace: $dices[numbers[6]], background: $backgrounds[6])
                        DiceView(diceFace: $dices[numbers[7]], background: $backgrounds[7])
                        DiceView(diceFace: $dices[numbers[8]], background: $backgrounds[8])
                        Spacer()
                    }
                }
                
                Spacer()
                Button(action: {
                    self.backgrounds[0] = Color.white
                    self.backgrounds[1] = Color.white
                    self.backgrounds[2] = Color.white
                    self.backgrounds[3] = Color.white
                    self.backgrounds[4] = Color.white
                    self.backgrounds[5] = Color.white
                    self.backgrounds[6] = Color.white
                    self.backgrounds[7] = Color.white
                    self.backgrounds[8] = Color.white
                    self.numbers[0] = Int.random(in: 0 ... self.dices.count - 1)
                    self.numbers[1] = Int.random(in: 0 ... self.dices.count - 1)
                    self.numbers[2] = Int.random(in: 0 ... self.dices.count - 1)
                    self.numbers[3] = Int.random(in: 0 ... self.dices.count - 1)
                    self.numbers[4] = Int.random(in: 0 ... self.dices.count - 1)
                    self.numbers[5] = Int.random(in: 0 ... self.dices.count - 1)
                    self.numbers[6] = Int.random(in: 0 ... self.dices.count - 1)
                    self.numbers[7] = Int.random(in: 0 ... self.dices.count - 1)
                    self.numbers[8] = Int.random(in: 0 ... self.dices.count - 1)
                    
                    if self.numbers[0] == self.numbers[1] && self.numbers[0] == self.numbers[2] {
                        self.initialAmount += self.bettingAmount * 50
                        self.backgrounds[0] = Color.black
                        self.backgrounds[1] = Color.black
                        self.backgrounds[2] = Color.black
                    }
                    else if self.numbers[3] == self.numbers[4] && self.numbers[3] == self.numbers[5]{
                        self.initialAmount += self.bettingAmount * 50
                        self.backgrounds[3] = Color.black
                        self.backgrounds[4] = Color.black
                        self.backgrounds[5] = Color.black
                    }
                    else if self.numbers[6] == self.numbers[7] && self.numbers[6] == self.numbers[8]{
                        self.initialAmount += self.bettingAmount * 50
                        self.backgrounds[6] = Color.black
                        self.backgrounds[7] = Color.black
                        self.backgrounds[8] = Color.black
                    }
                    else {
                        self.initialAmount -= self.bettingAmount
                    }
                }) {
                    Text("Spin")
                        .font(.largeTitle)
                        .fontWeight(.medium)
                        .foregroundColor(Color.white)
                        .padding(.all, 10)
                        .padding([.trailing, .leading], 20)
                        .background(Color.purple)
                        .cornerRadius(25)
                }
                
                Spacer()
            }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
